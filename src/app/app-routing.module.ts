import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth/guards/auth.guard';
import { DashboardLayoutComponent } from './shared/layouts/dashboard-layout/dashboard-layout.component';
import { HomeComponent } from './shared/pages/home/home.component';
import { LoginComponent } from './auth/components/login/login.component';
import { LogoutComponent } from './auth/components/logout/logout.component';
import { ProfileComponent } from './user/pages/profile/profile.component';
import { ProfileGuard } from './user/guards/profile.guard';
import { RoleGuard } from './auth/guards/role.guard';
import { SimpleLayoutComponent } from './shared/layouts/simple-layout/simple-layout.component';

const routes: Routes = [
  // dashboard routes goes here
  {
    path: '',
    component: DashboardLayoutComponent,
    canActivate: [AuthGuard, ProfileGuard],
    children: [
      {
        path: '',
        component: HomeComponent,
        pathMatch: 'full',
        canActivate: [AuthGuard],
      },
      {
        path: 'admin',
        component: HomeComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: { role: ['ROLE_ADMIN'] },
      },
      {
        path: 'student',
        loadChildren: () =>
          import('./student/student.module').then((m) => m.StudentModule),
        canActivate: [AuthGuard, RoleGuard],
        canActivateChild: [ProfileGuard],
        canLoad: [ProfileGuard],
        data: { role: ['ROLE_ADMIN', 'ROLE_STUDENT'] },
      },
      {
        path: 'user',
        loadChildren: () =>
          import('./user/user.module').then((m) => m.UserModule),
      },
    ],
  },

  // routes with simple layout
  {
    path: '',
    component: SimpleLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  // no layout routes
  { path: 'logout', component: LogoutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
