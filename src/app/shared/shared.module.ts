import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { AppRoutingModule } from '../app-routing.module';
import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { MessageDropdownComponent } from './components/message-dropdown/message-dropdown.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotificationDropdownComponent } from './components/notification-dropdown/notification-dropdown.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SimpleLayoutComponent } from './layouts/simple-layout/simple-layout.component';
import { UserDropdownComponent } from './components/user-dropdown/user-dropdown.component';

@NgModule({
  declarations: [
    DashboardLayoutComponent,
    FooterComponent,
    HomeComponent,
    MessageDropdownComponent,
    NavbarComponent,
    NotificationDropdownComponent,
    SidebarComponent,
    SimpleLayoutComponent,
    UserDropdownComponent,
  ],
  exports: [
    DashboardLayoutComponent,
    FooterComponent,
    HomeComponent,
    NavbarComponent,
    SidebarComponent,
    SimpleLayoutComponent,
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    NgxSkeletonLoaderModule,
    RouterModule,
  ],
})
export class SharedModule {}
