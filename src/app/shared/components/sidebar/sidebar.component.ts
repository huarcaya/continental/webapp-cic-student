import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/auth/services/auth.service';
import { AvatarService } from '../../services/avatar.service';
import { Config } from 'src/app/config/config';
import { Language } from '../../interfaces/language.interface';
import { Menu } from '../../models/menu';
import { MenusService } from '../../services/menus.service';
import { ProfileService } from 'src/app/user/services/profile.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  title = Config.APP.name;
  menuList: any[] = [];
  adminMenuList: any[] = [];
  userMenuList: any[] = [];
  employeeMenuList: any[] = [];
  studentMenuList: any[] = [];
  menus: any[] = [];
  profile!: Language;

  constructor(
    private avatarService: AvatarService,
    private menusService: MenusService,
    private profileService: ProfileService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.profile = this.profileService.profile;
    this.getMenu();
    this.menusService.getMenus().subscribe((resp) => (this.menus = resp));
  }

  hasRole(roleName: string): boolean {
    return this.authService.user.roles.includes(roleName);
  }

  getMenu(): void {
    this.authService.user.roles.forEach((role) => {
      const menu = {
        name: this.getMenuName(role),
        role,
        menu: this.getMenuList(role),
      };
      this.menuList.push(menu);
    });
  }

  getMenuName(role: string): string {
    switch (role) {
      case 'ROLE_ADMIN':
        return 'Admin';
      case 'ROLE_USER':
        return 'User';
      case 'ROLE_EMPLOYEE':
        return 'Office';
      case 'ROLE_STUDENT':
        return 'Student';
      default:
        return 'Menu';
    }
  }

  getMenuList(role: string): Menu[] {
    switch (role) {
      case 'ROLE_ADMIN':
        this.adminMenuList = Menu.ADMIN_MENU;
        return Menu.ADMIN_MENU;
      case 'ROLE_USER':
        this.userMenuList = Menu.USER_MENU;
        return Menu.USER_MENU;
      case 'ROLE_EMPLOYEE':
        this.employeeMenuList = Menu.EMPLOYEE_MENU;
        return Menu.EMPLOYEE_MENU;
      case 'ROLE_STUDENT':
        this.studentMenuList = Menu.STUDENT_MENU;
        return Menu.STUDENT_MENU;
      default:
        return [];
    }
  }

  avatar(): string {
    const text =
      this.authService.user.firstName[0] + ' ' + this.authService.user.lastName;
    return this.avatarService.generateAvatar(text);
  }
}
