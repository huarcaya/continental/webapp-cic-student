import { environment } from 'src/environments/environment';

export class Config {

  static readonly APP = {
    name: 'Centro de Idiomas',
    code: 'CICNGX'
  };
  static readonly API_URL = `${environment.apiEndPoint}`;
  static readonly CREDENTIALS = 'frontendapp' + ':' + '123456';

  constructor() { }
}
