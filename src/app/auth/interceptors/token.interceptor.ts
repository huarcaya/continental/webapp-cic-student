import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { AuthService } from '../services/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const token = this.authService.token;
    const refreshToken = this.authService.refreshToken;

    if (token != null && request?.body?.grant_type !== 'refresh_token') {
      const authRequest = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + token),
      });

      return next.handle(authRequest);
    }

    if (refreshToken && request.body.grant_type === 'refresh_token') {
      const authRequest = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + refreshToken),
      });

      return next.handle(authRequest);
    }

    return next.handle(request);
  }
}
