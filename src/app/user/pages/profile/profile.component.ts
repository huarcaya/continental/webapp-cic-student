import { Component, OnInit } from '@angular/core';

import { Language } from 'src/app/shared/interfaces/language.interface';
import { ProfileService } from '../../services/profile.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  contentLoaded: boolean = false;
  languages: Language[] = [];

  constructor(
    private profileService: ProfileService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getLanguages();
  }

  getLanguages(): void {
    this.userService.getLanguages().subscribe({
      next: (languages) => (this.languages = languages),
      error: (err) => (this.contentLoaded = true),
      complete: () => (this.contentLoaded = true),
    });
  }

  selectLanguage(language: Language): void {
    this.profileService.cleanProfile();
    this.profileService.profile = language;
    window.location.href = '/';
  }
}
