import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Config } from 'src/app/config/config';

@Injectable({
  providedIn: 'root',
})
export class GradesService {
  private baseEndpoint = `${Config.API_URL}grades`;

  constructor(private http: HttpClient) {}

  getLanguageGrades(languageId: string): Observable<any[]> {
    const url = `${this.baseEndpoint}/language/${languageId}`;
    return this.http.get<any[]>(url);
  }
}
