import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AcademicRecordComponent } from './pages/academic-record/academic-record.component';
import { AttendanceComponent } from './pages/attendance/attendance.component';

const routes: Routes = [
  {
    path: 'academic-record',
    component: AcademicRecordComponent,
  },
  {
    path: 'attendance',
    component: AttendanceComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentRoutingModule {}
